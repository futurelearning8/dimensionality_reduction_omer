import numpy as np
import pandas as pd
from tensorflow.keras.datasets import mnist
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
import time
import plotly.graph_objects as go
import pandas as pd


def timing(func):
    """wrapper provides timing of running functions"""
    def timing_wrapper(*args, **kwargs):
        start_time = time.time()
        res = func(*args, **kwargs)
        cycle_time = time.time() - start_time
        print(f"running time: {cycle_time}")
        return cycle_time, res
    return timing_wrapper


@timing
def perform_knn_calc(train_X_pca, test_X_scale, train_y):
    neigh_pca = KNeighborsClassifier(n_neighbors=3)
    neigh_pca.fit(train_X_pca, train_y)
    return neigh_pca

def perform_pca_loop(train_X, test_X, train_y, test_y, threshold_list):
    time=[]
    acc_list=[]
    for var_threshold in threshold_list:
        pca = PCA(n_components= var_threshold)
        train_X_pca = pca.fit_transform(train_X)
        test_X_scale = pca.transform(test_X)

        cycle_time,neigh_pca = perform_knn_calc(train_X_pca, test_X_scale, train_y)
        y_hat = neigh_pca.predict(test_X_scale)
        time.append(cycle_time)
        print(f"acc for NN after PCA={var_threshold}")
        curr_acc =accuracy_score(test_y, y_hat)
        acc_list.append(curr_acc)
        print(curr_acc)
    return time, acc_list


def main():
    (train_X, train_y), (test_X, test_y) = mnist.load_data()

    train_X = np.reshape(train_X,(train_X.shape[0],train_X.shape[1]*train_X.shape[2]))
    test_X = np.reshape(test_X,(test_X.shape[0],test_X.shape[1]*test_X.shape[2]))

    #section 2

    neigh = KNeighborsClassifier(n_neighbors=3)
    neigh.fit(train_X, train_y)
    y_hat = neigh.predict(test_X)
    # acc for NN
    print("acc for NN")
    print ( accuracy_score(test_y , y_hat))

    #PCA

    scaler = MinMaxScaler()
    scaler.fit(train_X)
    train_X_scale = scaler.transform(train_X)
    #section 3
    pca = PCA(n_components=0.95)
    train_X_scale = pca.fit_transform(train_X_scale)
    neigh_pca = KNeighborsClassifier(n_neighbors=3)
    neigh_pca.fit(train_X_scale, train_y)
    #section 4
    test_X_scale = scaler.transform(test_X)
    test_X_scale = pca.transform(test_X_scale)
    y_hat = neigh_pca.predict(test_X_scale)
    print("acc for NN after PCA")
    print ( accuracy_score(test_y , y_hat))

    #section 5
    # i guess KNN is especially sensitive to PCA since
    # after the PCA we have only few features with
    # high variance. calculating distance in order
    #to find nearest neighboor is much easier and faster
    #

    #section 6
    pca_thresh_list = [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95, 0.99]
    time_list, acc_list =perform_pca_loop(train_X, test_X, train_y, test_y, pca_thresh_list)

    fig = go.Figure()

    # Add traces
    fig.add_trace(go.Scatter(x=pca_thresh_list, y=acc_list,
                             mode='lines+markers',
                             name='accuracy / PCA threshold'))
    fig.add_trace(go.Scatter(x=pca_thresh_list, y=time_list,
                             mode='lines+markers',
                             name='time / PCA threshold'))


    fig.show()
if __name__ == '__main__':
    main()